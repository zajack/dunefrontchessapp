import { Controller, Get } from '@nestjs/common';

import { Message } from '@dunefront-interview/api-interfaces';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('user-name')
  getData(): Message {
    return this.appService.getData();
  }
}
