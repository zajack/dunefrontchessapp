import { Injectable } from '@nestjs/common';
import { Message } from '@dunefront-interview/api-interfaces';

@Injectable()
export class AppService {
  getData(): Message {
    return { message: 'DuneFront!' };
  }
}

