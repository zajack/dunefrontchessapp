import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, of, Subscription } from 'rxjs';
import { State } from '../+state/chess.reducer';
import { Store } from '@ngrx/store';
import { selectAvailableMoves, selectAvailablePawns, selectSelectedPawnPos } from '../+state/chess.selectors';
import { moveKnight, setSelectedPawnPos } from '../+state/chess.actions';

@Component({
  selector: 'df-chess-controls',
  templateUrl: './chess-controls.component.html',
  styleUrls: ['./chess-controls.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChessControlsComponent implements OnInit, OnDestroy {
  public availablePawns$: Observable<number[][]> = of([])
  public availableMoves$: Observable<number[][]> = of([]);

  public subs: Subscription[] = [];
  public moveFrom: string = '';
  public moveTo: string = '';

  constructor(private store: Store<State>) {}

  public ngOnInit() {
    this.availablePawns$ = this.store.select(selectAvailablePawns)
    this.availableMoves$ = this.store.select(selectAvailableMoves);
    this.trackSelectedPawn();
  }

  public ngOnDestroy(): void {
    this.subs.forEach(sub => {
      if (sub) {
        sub.unsubscribe();
      }
    });
  }

  public onFromChanged(position: string): void {
    const pawnPosition = position.length ? position.split(',').map(x => parseInt(x)) : [];
    this.store.dispatch(setSelectedPawnPos({pawnPosition}))
  }

  public move() {
    const newPosition = this.moveTo.split(',').map(x => parseInt(x));
    this.store.dispatch(moveKnight({newPosition}))
  }

  public readablePos(move: number[]) {
    const colLabels = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
    return `${move[0]} ${colLabels[move[1] - 1]}`;
  }

  private trackSelectedPawn(): void {
    this.subs.push(this.store.select(selectSelectedPawnPos)
    .subscribe(pos => {
      this.moveFrom = pos.join(',')
    }))
  }
}
