import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from '@dunefront-interview/api-interfaces';

@Component({
  selector: 'df-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  userName$ = this.http.get<Message>('/api/user-name');

  constructor(private http: HttpClient) {}
}
