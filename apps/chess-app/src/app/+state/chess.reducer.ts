import { on, createReducer } from '@ngrx/store';
import { ChessField } from '../services/controls.service';
import {
  resetMatrixSuccess,
  setAvailableMoves,
  setAvailablePawnsSuccess,
  setSelectedPawnPos,
  updateMatrix
} from './chess.actions';

export const featureKey = 'chess'

export interface State {
  matrix: ChessField[][];
  selectedPawnPos: number[];
  availablePawnMoves: number[][];
  availablePawnsPos: number[][];
}

export const initialState: State = {
  matrix: [],
  selectedPawnPos: [],
  availablePawnMoves: [],
  availablePawnsPos: []
};

export const ChessReducer = createReducer(
  initialState,
  on(resetMatrixSuccess, (state, action) => ({
    ...state,
    matrix: action.matrix
  })),
  on(updateMatrix, (state, action) => ({
    ...state,
    matrix: action.matrix
  })),
  on(setSelectedPawnPos, (state,action) => ({
    ...state,
    selectedPawnPos: action.pawnPosition
  })),
  on(setAvailableMoves, (state, action) => ({
    ...state,
    availablePawnMoves: action.availableMoves
  })),
  on(setAvailablePawnsSuccess, (state, action) => ({
    ...state,
    availablePawnsPos: action.availablePawnsPos
  }))
);


