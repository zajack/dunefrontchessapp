import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, withLatestFrom, concatMap, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { ControlsService } from '../services/controls.service';
import {
  checkGameState,
  moveKnight,
  resetMatrix,
  resetMatrixSuccess,
  setAvailableMoves, setAvailablePawns, setAvailablePawnsSuccess,
  setSelectedPawnPos,
  updateMatrix
} from './chess.actions';
import { Store } from '@ngrx/store';
import { State } from './chess.reducer';
import { selectAvailablePawns, selectMatrix, selectSelectedPawnPos } from './chess.selectors';
import { TimerServiceService } from '../services/timer-service.service';

@Injectable()
export class ChessEffects {

  resetMatrix$ = createEffect(() =>
    this.actions$.pipe(
      ofType(resetMatrix),
      switchMap(() =>  of(this.controls.resetChessMatrix())),
      switchMap(matrix => {
        this.timer.resetTimer();
        return [
          resetMatrixSuccess({matrix}),
          setAvailablePawns()
        ]
      })
    )
  );

  setSelectedPawn$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setSelectedPawnPos),
      withLatestFrom(this.store$.select(selectSelectedPawnPos), this.store$.select(selectMatrix)),
      concatMap(([action, selectedPawnPos, matrix]) => {
        const availableMoves = this.controls.getAvailableMoves(action.pawnPosition, selectedPawnPos, matrix);
        const newMatrix = this.controls.highlightAvailableMoves(matrix, availableMoves);
        return of({availableMoves, newMatrix})
      }),
      switchMap((res)=> [
          setAvailableMoves({availableMoves: res.availableMoves}),
          updateMatrix({matrix: res.newMatrix})
        ]
      )
    )
  );

  moveKnight$ = createEffect(() =>
    this.actions$.pipe(
      ofType(moveKnight),
      withLatestFrom(this.store$.select(selectSelectedPawnPos), this.store$.select(selectMatrix)),
      concatMap(([action, selectedPawnPos, matrix]) => {
        const newMatrix = this.controls.moveKnight(action.newPosition, selectedPawnPos, matrix);
        return of(newMatrix)
      }),
      switchMap((res) =>[
          updateMatrix({matrix: res}),
          setAvailableMoves({availableMoves: []}),
          setSelectedPawnPos({pawnPosition: []}),
          setAvailablePawns()
        ]
      )
    )
  )

  setAvailablePawns$ = createEffect(() =>
    this.actions$.pipe(
      ofType(setAvailablePawns),
      withLatestFrom(this.store$.select(selectMatrix)),
      concatMap(([action, matrix]) => {
        const availablePawnsPos = this.controls.getAvailablePawnsPos(matrix);
        return of({availablePawnsPos})
      }),
      concatMap(res => [
        setAvailablePawnsSuccess({availablePawnsPos: res.availablePawnsPos}),
        checkGameState()
      ])
  ))

  checkGameState$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(checkGameState),
      withLatestFrom(this.store$.select(selectAvailablePawns),  this.store$.select(selectMatrix)),
      map(([action, pawns, matrix]) => {
        if(pawns.length < 3) {
          const result = this.controls.checkGameState(pawns, matrix)
          if(result) {
            alert(`Player ${result} won!`);
            this.store$.dispatch(resetMatrix())
          }
        }
        return of(null)
      }));
  }, {
    dispatch: false
  });

  constructor(
    private actions$: Actions,
    private store$: Store<State>,
    private controls: ControlsService,
    private timer: TimerServiceService
  ) {
  }
}
