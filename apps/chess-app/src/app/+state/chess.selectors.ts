import { featureKey, State } from './chess.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const selectAppState = createFeatureSelector<State>(featureKey);
export const selectMatrix = createSelector(selectAppState, state => state.matrix);
export const selectSelectedPawnPos = createSelector(selectAppState, state => state.selectedPawnPos);
export const selectAvailableMoves = createSelector(selectAppState, state => state.availablePawnMoves);
export const selectAvailablePawns = createSelector(selectAppState, state => state.availablePawnsPos);
