import { createAction, props } from '@ngrx/store';
import { ChessField } from '../services/controls.service';

export const updateMatrix = createAction(
  '[Chess] update chessMatrix',
  props<{matrix: ChessField[][]}>()
);
export const resetMatrix = createAction(
  '[Chess] reset chessMatrix ',
)
export const resetMatrixSuccess = createAction(
  '[Chess] reset chessMatrix success',
  props<{matrix: ChessField[][]}>()
);

export const setSelectedPawnPos = createAction(
  '[Chess] set selected pawn position',
  props<{pawnPosition: number[]}>()
)

export const setAvailableMoves = createAction(
  '[Chess] set available pawn moves',
  props<{availableMoves: number[][]}>()
)

export const setAvailablePawns = createAction(
  '[Chess] set available pawns positions',
)
export const setAvailablePawnsSuccess = createAction(
  '[Chess] set available pawns positions success',
  props<{availablePawnsPos: number[][]}>()
)

export const moveKnight = createAction(
  '[Chess] move knight',
  props<{newPosition: number[]}>()
)

export const checkGameState = createAction(
  '[Chess] Check game state'
)
