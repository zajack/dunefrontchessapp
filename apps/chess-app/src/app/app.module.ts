import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ChessBoardComponent } from './chess-board/chess-board.component';
import { ChessControlsComponent } from './chess-controls/chess-controls.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { ChessReducer } from './+state/chess.reducer';
import { ChessEffects } from './+state/chess.effects';

@NgModule({
  declarations: [AppComponent, ChessBoardComponent, ChessControlsComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    CommonModule,
    FormsModule,
    StoreModule.forRoot({ chess: ChessReducer }),
    EffectsModule.forRoot([ChessEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
