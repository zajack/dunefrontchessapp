import { Injectable } from '@angular/core';
import { TimerServiceService } from './timer-service.service';

export interface ChessField {
  knightTeam: number;
  isPlayable: boolean;
  label?: string;
  highlight: boolean
}

@Injectable({
  providedIn: 'root'
})
export class ControlsService {

  constructor(private timer: TimerServiceService) {}


  public getAvailableMoves(selectedPawnPos: number[], selPawnPos: number[], matrix: ChessField[][]): number[][] {
    const [i, j] = selectedPawnPos;

    if (!selPawnPos.length) {
      return [];
    }

    const moves = [[i - 2, j - 1], [i - 1, j - 2], [i - 2, j + 1], [i - 1, j + 2],
      [i + 2, j - 1], [i + 1, j - 2], [i + 2, j + 1], [i + 1, j + 2]
    ].filter(pos => pos[0] > 0 && pos[1] > 0 && pos[0] < 9 && pos[1] < 9);

    const selKnightTeam = matrix[selPawnPos[0]][selPawnPos[1]].knightTeam;

    return moves.filter(pos => matrix[pos[0]][pos[1]].knightTeam !== selKnightTeam);
  };


  public resetChessMatrix(): ChessField[][] {
    const colLabels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
    const defaultSquare: ChessField = { isPlayable: false, label: '', knightTeam: 0, highlight: false };
    const chessMatrix: ChessField[][] = [];
    for (let i = 0; i < 10; i++) {
      chessMatrix.push([]);
      for (let j = 0; j < 10; j++) {
        const square = { ...defaultSquare };
        if (i === 0 || i === 9) {
          square.label = colLabels[j - 1] || '';
        } else if (j === 0 || j === 9) {
          square.label = i === 0 || i === 9 ? '' : i.toString();
        } else {
          square.isPlayable = true;
        }
        chessMatrix[i][j] = square;
      }
    }
    //default knights positions
    chessMatrix[8][7].knightTeam = 1;
    chessMatrix[8][2].knightTeam = 1;
    chessMatrix[1][7].knightTeam = 2;
    chessMatrix[1][2].knightTeam = 2;

    return chessMatrix;
  }


  public moveKnight(position: number[], selectedPawnPos: number[], matrix: ChessField[][]): ChessField[][]{
    this.timer.start();
    const knightSquare = matrix[selectedPawnPos[0]][selectedPawnPos[1]];

    return matrix.map((row, rowIdx) => {
      return row.map((col, colIdx) => {
        let knightTeam = null;
        if( rowIdx === position[0] && colIdx === position[1]) {
          knightTeam = knightSquare.knightTeam;
        } else if (rowIdx === selectedPawnPos[0] && colIdx === selectedPawnPos[1]) {
          knightTeam = 0
        }
        return knightTeam !== null ? { ...col, knightTeam: knightTeam} : col
      });
    });
  }

  public highlightAvailableMoves(matrix: ChessField[][], moves: number[][]): ChessField[][] {
    return matrix.map((row, rowIdx) => {
      return row.map((col, colIdx) => {
        return { ...col, highlight: moves.findIndex(move => move[0] === rowIdx && move[1] === colIdx) !== -1 };
      });
    });
  }

  public getAvailablePawnsPos(matrix: any) {
    let positions = []
    for (let i = 0; i < matrix.length; i++) {
      for (let j = 0; j < matrix[i].length; j++) {
        if (matrix[i][j].knightTeam) {
          positions.push([i, j]);
        }
      }
    }
    return positions;
  }

  public checkGameState(pawnsPositions: number[][], matrix: ChessField[][]): number | void {
    const pawns: number[] = pawnsPositions.map(pawn => matrix[pawn[0]][pawn[1]].knightTeam);
    if(pawns.length === 1 || pawns.length !== new Set(pawns).size) {
      return pawns[0]
    }
  }
}
