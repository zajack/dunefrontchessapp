import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimerServiceService {
  public timer = new BehaviorSubject<number>(0);
  private isTimerStarted: boolean = false;
  private interval: number = 0;

  public start(): void {
    if (!this.isTimerStarted) {
      this.isTimerStarted = true
      let counter = 1;
      this.interval = setInterval(() => {
        this.timer.next(counter++);
      }, 1000);
    }
  }

  public getTime(): Observable<number> {
    return this.timer;
  }

  public resetTimer(): void {
    this.isTimerStarted = false
    clearInterval(this.interval)
    this.timer.next(0)
  }
}
