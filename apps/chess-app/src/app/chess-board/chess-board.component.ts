import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TimerServiceService } from '../services/timer-service.service';
import { ChessField } from '../services/controls.service';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '../+state/chess.reducer';
import { moveKnight, resetMatrix, setSelectedPawnPos } from '../+state/chess.actions';
import { selectMatrix } from '../+state/chess.selectors';

@Component({
  selector: 'df-chess-board',
  templateUrl: './chess-board.component.html',
  styleUrls: ['./chess-board.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChessBoardComponent implements OnInit {
  public chessMatrix$: Observable<ChessField[][]> = of([]);
  public time$: Observable<number> = of(0);

  constructor(
    private timer: TimerServiceService,
    private store$: Store<State>
  ) {
  }

  ngOnInit(): void {
    this.chessMatrix$ = this.store$.select(selectMatrix)
    this.time$ = this.timer.getTime();
    this.store$.dispatch(resetMatrix());
  }

  public isDarkSquare(rowId: number, colId: number): boolean {
    return rowId % 2 === 0 && colId % 2 === 0 || rowId % 2 !== 0 && colId % 2 !== 0;
  }

  public onSquareClicked(rowId: number, colId: number, col: ChessField) {
    let pawnPosition: number[] = [];
    if (col.knightTeam) {
      pawnPosition = [rowId, colId];
    }
    this.store$.dispatch(setSelectedPawnPos({ pawnPosition }));
  }

  public moveKnight(rowId: number, colId: number) {
    this.store$.dispatch(moveKnight({newPosition: [rowId, colId]}))
  }
}
