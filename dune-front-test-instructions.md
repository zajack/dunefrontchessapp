# Test Instructions

## Setup

Ensure Node.js (which has npm built in) is installed on the machine.

In PowerShell go to this folder then run:
    npm install 

To start application run:

    npm run start  (to start client)

    npm run start:api  (to start backend)

## Introduction

You've just started to prototype a chess game and you've got as far as displaying knights.

It's currently only possible to move one of the knights one valid move only, 
but that move is hardcoded and the board square references in the input boxes are ignored.

## Objective

The objective of the test is just fix few bugs in this app and to implement moving the other knights.

This means
 - complete board layout - should display all 8 rows
 - implement communication between chess-board and chess-controls components
 - fix problem with displaying user name (Hello: ?)
 - fix problem with timer - should display time in seconds triggered by clicking move button  
 - interpreting the moveFrom and moveTo strings and making the move code more generic
 - only moving if the moveFrom identifies one of the four knights and the moveTo is a valid move for that knight

